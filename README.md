
# ReadMe

To install do I'd recommend the use of a virtualenv. The use of a virtual environment is not mandatory but is described bellow :

    $ sudo apt install virtualenv	# Install virtualenv
    $ virtualenv -p python3.5 venv 	# Creation of the virtual environment
    $ source ./venv/bin/activate	# Load the virtual environment
 
 After this do the following **inside the Producer's Folder:**

    pip install -r requirements.txt

