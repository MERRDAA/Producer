from kafka import KafkaProducer
import random
import time
import json
import calendar;

try:
    producer = KafkaProducer(bootstrap_servers='deti-engsoft-01.ua.pt:9092')
except:
    print("Exception")

print("Start Sending")
while True:
    ts = time.time()
    dic1 = {"sensor": "branquinho", "volume": round(random.random(), 2), "timestamp:": ts}
    dic2 = {"sensor": "ramalhinho", "volume": round(random.random(), 2), "timestamp:": ts}
    dic3 = {"sensor": "luisinho",   "volume": round(random.random(), 2), "timestamp:": ts}

    producer.send("401_all", json.dumps(dic1).encode("utf-8"))
    producer.send("401_all", json.dumps(dic2).encode("utf-8"))
    producer.send("401_all", json.dumps(dic3).encode("utf-8"))

    print("Messages sent: \n{}\n{}\n{}".format(str(dic1), str(dic2), str(dic3)))
    time.sleep(5)
